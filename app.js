// Book Class: Represents a Book
class Book {
    constructor(title, author, isbn){
        this.title = title;
        this.author = author;
        this.isbn = isbn;
    }
}

// UI Class: Handle UI Task
class UI {
    static displayBooks() {
        const books = Store.getBooks();

        books.forEach(book => UI.addBookToList(book))
    }

    static addBookToList(book) {
        const list = document.querySelector('#book-list');
        const bookRow = document.createElement('tr');

        bookRow.innerHTML = `
            <td>${book.title}</td>
            <td>${book.author}</td>
            <td>${book.isbn}</td>
            <td>
                <a href="#" class="btn btn-danger btn-sm delete">X</a>
                <a href="#" data-isbn="${book.isbn}" class="btn btn-info btn-sm edit">
                    <i class="fa fa-edit"></i>
                </a>
            </td>
        `;

        list.appendChild(bookRow);
    }

    static showAlert(message, className) {
        const div = document.createElement('div');
        div.className = `alert alert-${className}`;
        div.appendChild(document.createTextNode(message));
        const colForm = document.querySelector('#col-form');
        const form = document.querySelector('#book-form');
        colForm.insertBefore(div, form);

        //Vanish in 3 seconds
        setTimeout(() => document.querySelector('.alert').remove(), 3000);
    }

    static clearFields() {
        document.querySelector('#title').value = '';
        document.querySelector('#author').value = '';
        document.querySelector('#isbn').value = '';
    }

    static deleteBook(el) {
        if(el.classList.contains('delete')) {
            el.parentElement.parentElement.remove();
        }
    }
}

//Store Class: Handles Storage
class Store {
    static getBooks() {
        let books;
        if (localStorage.getItem('books') === null) {
            books = [];
        } else {
            books = JSON.parse(localStorage.getItem('books'));
        }

        return books;
    }

    static addBook(book){
        const books = Store.getBooks();
        books.push(book);
        localStorage.setItem('books', JSON.stringify(books));
    }

    static getBook(isbn) {
        const books = Store.getBooks();
        return books.find( book => {
            if (book.isbn === isbn) {
                return book;
            }
        });
    }

    static updateBook(fields) {
        const books = Store.getBooks();
        books.forEach( book => {
            if (book.isbn === fields.isbn) {
                book.title = fields.title;
                book.author = fields.author;
            }
        });
        localStorage.setItem('books', JSON.stringify(books));
    }

    static removeBook(isbn){
        const books = Store.getBooks();
        books.forEach( (book, index) => {
            if (book.isbn === isbn) {
                books.splice(index, 1);
            }
        })

        localStorage.setItem('books', JSON.stringify(books));
    }
}

//Event: Display Books
document.addEventListener('DOMContentLoaded', UI.displayBooks)

//Event: Add a Book
document.querySelector('#book-form').addEventListener('submit', e => {
    //Prevent actual submit
    e.preventDefault();

    //Get form values
    const title = document.querySelector('#title').value;
    const author = document.querySelector('#author').value;
    const isbn = document.querySelector('#isbn').value;

    // Validate
    if(title === '' || author === '' || isbn === ''){
        UI.showAlert('Please fill in all fields', 'danger');
    } else if(Store.getBook(isbn) === undefined){
        // Instatiate book
        const book = new Book(title, author, isbn);
    
        // Add book to list UI
        UI.addBookToList(book);

        // Add book to Store
        Store.addBook(book);
    
        // Show success message
        UI.showAlert('Book Added', 'success');

        // Clear fields
        UI.clearFields();
    } else {
        const fields = {
            title, 
            author, 
            isbn
        }
        Store.updateBook(fields);
        window.location.reload();
        UI.showAlert('Book Updated', 'success');
    }

});

// Event: Remove a Book
document.querySelector('#book-list').addEventListener('click', e => {
    e.stopPropagation();
    let action = e.target.classList;

    if(action.contains('delete')) {
        // Remove from UI
        UI.deleteBook(e.target)
    
        // Remove book from store
        Store.removeBook(e.target.parentElement.previousElementSibling.textContent);
        // Show success message
        UI.showAlert('Book Removed', 'success');
    } else {
        let book = Store.getBook(e.target.dataset.isbn);
        document.querySelector('#title').value = book.title;
        document.querySelector('#author').value = book.author;
        document.querySelector('#isbn').value = book.isbn;
        document.querySelector('input[type="submit"]').value = 'Update Book';
    }
});